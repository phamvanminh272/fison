from sqlalchemy import create_engine, Column, String, Integer, Numeric
from sqlalchemy.orm import Session, declarative_base

Base = declarative_base()

engine = create_engine(
    "postgresql+psycopg2://postgres:12345678@fison.ctmjkvo11d39.us-west-2.rds.amazonaws.com:5432/fison"
)


class Products(Base):
    __tablename__ = "products"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    quantity = Column(Integer)
    price = Column(Integer)
    code = Column(String)
    pictures = Column(String)
    industry_id = Column(Integer)


class ProductCategories(Base):
    __tablename__ = "product_categories"
    product_id = Column(Integer, primary_key=True)
    category_id = Column(Integer, primary_key=True)


class Attributes(Base):
    __tablename__ = "attributes"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    value = Column(String)
    industry_id = Column(Integer)


class ProductAttributes(Base):
    __tablename__ = "product_attributes"
    product_id = Column(Integer, primary_key=True)
    attribute_id = Column(Integer, primary_key=True)
    pictures = Column(String)
    price = Column(Integer)
