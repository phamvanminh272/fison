import json

from sqlalchemy.orm import Session

from orm import Products, engine, ProductCategories, ProductAttributes, Attributes


def lambda_handler(event, context):
    print(event["queryStringParameters"])
    paramaters = event["queryStringParameters"]
    if paramaters.get("product_id", None):
        response = get_attributes(paramaters.get("product_id"))
    else:
        category_id = int(paramaters["category_id"])
        response = get_products(category_id)
    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
        },
        "body": json.dumps(response),
    }


def get_attributes(product_id):
    with Session(engine) as session:
        product_attributes = (
            session.query(ProductAttributes, Attributes)
            .filter(
                ProductAttributes.attribute_id == Attributes.id,
                ProductAttributes.product_id == product_id,
            )
            .all()
        )
        attributes = {}
        for i in product_attributes:
            product_attribute_obj = i[0]
            attribute_obj = i[1]
            item = {
                    "value": attribute_obj.value,
                    "pictures": product_attribute_obj.pictures,
                    "price": product_attribute_obj.price
                }
            if attribute_obj.name in attributes.keys():
                attributes[attribute_obj.name].append(item)
            else:
                attributes[attribute_obj.name] = [item]
        attributes_list = [{"name": key, "value": value} for key, value in attributes.items()]
        print(attributes_list)
    return attributes_list


def get_products(category_id):
    # session = boto3.Session(
    #         aws_access_key_id='AKIA4GCS4YLRWQPUC7UP',
    #         aws_secret_access_key='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'
    #     )
    # s3 = session.resource('s3')
    # my_bucket = s3.Bucket('fison')
    pictures_folder = "https://s3.us-west-2.amazonaws.com/www.fison.store/pictures"
    with Session(engine) as session:
        products = session.query(Products).all()
        product_categories = (
            session.query(ProductCategories)
            .filter(ProductCategories.category_id == category_id)
            .all()
        )
        product_ids = [i.product_id for i in product_categories]
        products_list = []
        for i in products:
            if category_id != 0 and i.id not in product_ids:
                continue
            item = i.__dict__
            item.pop("_sa_instance_state")
            # files_list = []
            # for i in my_bucket.objects.filter(Prefix=f"pictures/{item['code']}/"):
            #     files_list.append(i.key)
            item["pictures"] = f"{pictures_folder}/{item['code']}/{item['code']}.avif"
            products_list.append(item)
    return products_list

event = {"queryStringParameters": {"product_id": 1}}
lambda_handler(event, "")
