import json

from orm import engine, Categories
from sqlalchemy.orm import Session


def lambda_handler(event, context):
    with Session(engine) as session:
        categories = session.query(Categories).all()
        categories_dict = {}
        for i in categories:
            if i.super_category_name in categories_dict.keys():
                categories_dict[i.super_category_name].append(
                    {"id": i.id, "name": i.name}
                )
            else:
                categories_dict[i.super_category_name] = [{"id": i.id, "name": i.name}]
    categories_list = []
    for i, v in categories_dict.items():
        categories_list.append({i: v})
    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
        },
        "body": json.dumps(categories_list),
    }
