from sqlalchemy import create_engine, Column, String, Integer, Numeric
from sqlalchemy.orm import Session, declarative_base

Base = declarative_base()

engine = create_engine(
    "postgresql+psycopg2://postgres:12345678@fison.ctmjkvo11d39.us-west-2.rds.amazonaws.com:5432/fison"
)


class Categories(Base):
    __tablename__ = "categories"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    super_category_name = Column(String)
